﻿/*
    The following license supersedes all notices in the source code.
*/

/*
    Copyright (c) 2016 Kurt Dekker/PLBM Games All rights reserved.

    http://www.twitter.com/kurtdekker
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:
    
    Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    
    Neither the name of the Kurt Dekker/PLBM Games nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
    TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using UnityEngine;
using System.Collections;

public class mainmenu : MonoBehaviour
{
	void Start ()
	{
		// This holds everything about what you want to do.
		// You could make a bunch of these structures and
		// select one randomly.

		StreamImageData sid = new StreamImageData ();
		sid.ImageURL = "http://plbm.com/wp-content/uploads/2016/09/kurtvert_example.jpg";
		sid.DestinationURL = "http://www.plbm.com";
		sid.NoButtonText = "No Thanks!";
		sid.YesButtonText = "Show Me!";
		sid.ContinueAction = () => {
			DoStartNewGame ();
		};

		// here's how to get a local image from your computer
		// sid.ImageURL = "file:///users/kurt/Documents/landscape.png";
		// sid.ImageURL = "file:///users/kurt/Documents/portrait.png";

		// call this at the start of your main menu to "stage up" the image.
		StreamImage.Instance ().PreDownloadNextImage ( sid);
	}

	void DoStartNewGame()
	{
		Application.LoadLevel ("playing");
	}

	bool DoPlayButtonPressed;

	void Update()
	{
		if (DoPlayButtonPressed)
		{
			bool showKurtVert = true;

			// <WIP> you might want to put some logic in here
			// that only shows it every other time. Right now
			// it will show it every single time to play, if
			// the image is ready and downloaded.

			if (showKurtVert)
			{
				StreamImage.Instance ().ShowQueuedImageAndContinue ();
				return;
			}
			// or maybe just go straight to playing
			DoStartNewGame ();
		}
	}

	// this is your mainmenu
	void OnGUI()
	{
		Rect r = new Rect( Screen.width * 0.20f, Screen.height * 0.10f, Screen.width * 0.60f, Screen.height * 0.10f);
		GUI.Label( r, "Main Menu", OurStyles.LABELCJ( 16));

		float yspacing = r.height * 1.5f;

		r.y += yspacing;
		GUI.Label( r, "This program demonstrates lazily loading an image\n" +
		          "from web URL and displaying it before playing.", OurStyles.LABELCJ( 12));

		r.y += yspacing;
		GUI.Label( r, "The upper right corner is StreamImage status.\n" +
		          "You can hide it in StreamImage.", OurStyles.LABELCJ( 12));
		
		r.y += yspacing;
		if (GUI.Button( r, "PLAY GAME", OurStyles.BUTTONBASE( 16)))
		{
			DoPlayButtonPressed = true;
		}
	}
}
