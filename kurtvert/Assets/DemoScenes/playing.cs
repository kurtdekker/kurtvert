﻿/*
    The following license supersedes all notices in the source code.
*/

/*
    Copyright (c) 2016 Kurt Dekker/PLBM Games All rights reserved.

    http://www.twitter.com/kurtdekker
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:
    
    Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    
    Neither the name of the Kurt Dekker/PLBM Games nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
    TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using UnityEngine;
using System.Collections;

public class playing : MonoBehaviour
{
	// this is your main game running...
	IEnumerator Start()
	{
		GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
		// this is the main game loop
		while(true)
		{
			cube.transform.localRotation = Quaternion.Euler (0, Time.time * 200, 0);
			yield return null;
		}
	}

	void DoQuit()
	{
		Application.LoadLevel ("mainmenu");
	}

	void OnGUI()
	{
		Rect r = new Rect( Screen.width * 0.20f, Screen.height * 0.10f, Screen.width * 0.60f, Screen.height * 0.10f);
		GUI.Label( r, "You are playing the game", OurStyles.LABELCJ( 16));

		r = new Rect (Screen.width * 0.01f, Screen.height * 0.01f, Screen.width * 0.15f, Screen.height * 0.15f);
		if (GUI.Button( r, "QUIT", OurStyles.BUTTONBASE( 16)))
		{
			DoQuit();
		}
	}
}
