﻿/*
    The following license supersedes all notices in the source code.
*/

/*
    Copyright (c) 2016 Kurt Dekker/PLBM Games All rights reserved.

    http://www.twitter.com/kurtdekker
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:
    
    Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    
    Neither the name of the Kurt Dekker/PLBM Games nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
    TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using UnityEngine;
using System.Collections;

public class StreamImage : MonoBehaviour
{
	// if you rename this script's "home" scene,
	// you need to change this name
	const string s_WhereThisScriptRuns = "StreamImageScene";

	// disable this block to hide the status printout
	public bool ShowStatus = true;

	// we don't even display buttons until this time passes
	const float MinimumDisplayTime = 2.0f;

	float displayTimer;

	static StreamImage _instance;

	public static StreamImage Instance()
	{
		if (!_instance)
		{
			_instance = new GameObject( "StreamImage singleton").AddComponent<StreamImage>();
		}
		return _instance;
	}

	void Awake()
	{
		if (_instance)
		{
			Destroy (_instance.gameObject);
			return;
		}
		DontDestroyOnLoad (gameObject);
	}

	StreamImageData SID;

	Texture2D QueuedImage;

	IEnumerator GetThisImage()
	{
		QueuedImage = null;
		using (WWW www = new WWW( SID.ImageURL))
		{
			yield return www;

			if (www.error == null)
			{
				Debug.Log("StreamImage Ok!");

				QueuedImage = www.texture;
			}
			else
			{
				Debug.Log("StreamImage Error: "+ www.error);
				hadError = www.error;
			}
		}
	}

	bool ActiveAndDisplayingPicture;
	bool ContinueToNextScreen;

	void Reset()
	{
		SID = null;
		ActiveAndDisplayingPicture = false;
		ContinueToNextScreen = false;
		QueuedImage = null;
		hadError = null;
		displayTimer = 0.0f;
	}

	public void PreDownloadNextImage( StreamImageData sid)
	{
		Reset ();
		SID = sid;
		StartCoroutine (GetThisImage ());
	}

	void DoContinueAction()
	{
		SID.ContinueAction ();
		Reset ();
	}

	public void ShowQueuedImageAndContinue()
	{
		// check if the queueing operation had been successful
		if (QueuedImage == null)
		{
			Debug.LogWarning( "StreamImage: ShowQueuedImageAndContinue(): no image queued");
			DoContinueAction ();
			return;
		}

		ContinueToNextScreen = false;

		// this activates the display process
		ActiveAndDisplayingPicture = true;

		displayTimer = 0;

		Application.LoadLevel (s_WhereThisScriptRuns);
	}

	void Update()
	{
		if (ActiveAndDisplayingPicture)
		{
			displayTimer += Time.deltaTime;

			if (ContinueToNextScreen)
			{
				DoContinueAction();
			}
		}
	}

	string hadError;

	void OnGUI_Status()
	{
		// Disable this status update block; it is just for debugging
		string statusString = "Ready";
		Color statusColor = Color.green;
		
		if (SID == null)
		{
			statusString = "OFF";
			statusColor = Color.yellow;
		}
		else
		{
			if (QueuedImage == null)
			{
				statusString = "Loading";
				statusColor = Color.yellow;
			}
		}
		
		if (ActiveAndDisplayingPicture && QueuedImage != null)
		{
			statusString = "ON!";
			statusColor = Color.green;
		}
		
		if (hadError != null)
		{
			statusString = "Error:" + hadError;
			statusColor = Color.red;
		}

		Rect r = new Rect( Screen.width * 0.49f, Screen.height * 0.01f,
		                  Screen.width * 0.50f, Screen.height * 0.06f);
		GUI.color = statusColor;
		GUI.Label ( r, statusString, OurStyles.LABELRJ (12));
	}

	void OnGUI_Display()
	{
		GUI.color = Color.white;
		
		if (ActiveAndDisplayingPicture)
		{
			GUI.color = Color.white;
			
			Rect r = new Rect( 0, 0, Screen.width, Screen.height);
			
			// fit image to screen horizontally or vertically and center it up
			if (QueuedImage.width * Screen.height < QueuedImage.height * Screen.width)
			{
				r.width = (r.height * QueuedImage.width) / QueuedImage.height;
				r.x = (Screen.width - r.width) / 2;
			}
			else
			{
				r.height = (r.width * QueuedImage.height) / QueuedImage.width;
				r.y = (Screen.height - r.height) / 2;
			}
			
			GUI.DrawTexture( r, QueuedImage);
		}
		else
		{
			// somehow we got flipped on and there is no graphic, so we
			// just hit the action button and get outa here...
			ContinueToNextScreen = true;
		}
		
		if (displayTimer >= MinimumDisplayTime)
		{
			GUI.color = Color.white;
			
			Rect r = new Rect( Screen.width * 0.10f, Screen.height * 0.85f,
			                  Screen.width * 0.30f, Screen.height * 0.10f);
			
			int fontsize = 12;
			
			if (GUI.Button( r, "No thanks!", OurStyles.BUTTONBASE(fontsize)))
			{
				ContinueToNextScreen = true;
			}
			
			r.x = Screen.width * 0.60f;
			if (GUI.Button( r, "Show Me!", OurStyles.BUTTONBASE(fontsize)))
			{
				Application.OpenURL( SID.DestinationURL);
				
				ContinueToNextScreen = true;
			}
		}		
	}

	void OnGUI()
	{
		if (SID != null)
		{
			OnGUI_Display();
		}
		if (ShowStatus)
		{
			OnGUI_Status();
		}
	}
}
